#ifndef BMP_INCLUDED
#define BMP_INCLUDED

#include <stdio.h>

/*/You will recognize this from assignment #1
//Take note: Pixels are RGB (There is NO alpha)*/

/*/Image Data Stucts////////////////////////////////*/

typedef struct {
  unsigned char r, g, b;
} Pixel;


typedef struct {
  int width, height;
  Pixel *pixels;
} Image;

/*//////////////////////////////////////////////////*/



/*/Image utility functions//////////////////////////*/

Image *ImageNew(int width, int height);
void ImageFree(Image **img);
void ImageCopy(Image *src, Image *dst);
int ImageIsValid(Image *img);
Pixel *ImageGetPixel(Image *img, int x, int y);
void ImageSetPixel(Image *img, int x, int y, Pixel *p);

/*//////////////////////////////////////////////////*/



Image *BMPReadImage(FILE *fp);
/* Reads an image from a BMP file. */

void BMPWriteImage(Image *img, FILE *fp);
/* Writes an image to a BMP file. */



#endif /* BMP_INCLUDED */
