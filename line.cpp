#include <stdio.h>
#include <stdlib.h>
#include "line.h"

Line::Line(FILE* fp,int* index){
  if(fscanf(fp," %d %lg %lg %lg %lg %lg %lg",index,
	    &(start[0]),&(start[1]),&(start[2]),
	    &(end[0]),&(end[1]),&(end[2])) != 7){
    fprintf(stderr, "Failed to parse shape_line for Line\n"); 
    exit(EXIT_FAILURE);
  }
}
Line::Line(Ray ray,Material* mat){
  material=mat;
  start=ray.p;
  end=ray.d;
}
char* Line::name(void){return "line";}
void  Line::write(int indent,FILE* fp){
  int i;
  for(i=0;i<indent;i++){fprintf(fp," ");}
  fprintf(fp,"#shape_line %d\n",material->index);
  for(i=0;i<indent;i++){fprintf(fp," ");}
  fprintf(fp,"  %lg %lg %lg\n",start[0],start[1],start[2]);
  for(i=0;i<indent;i++){fprintf(fp," ");}
  fprintf(fp,"  %lg %lg %lg\n",end[0],end[1],end[2]);
}
Flt Line::intersect(Ray ray,IntersectionInfo& iInfo){return -1.0;}

BoundingBox Line::getBoundingBox(void){
  return BoundingBox(start,end);
}
