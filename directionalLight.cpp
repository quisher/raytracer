#include <stdio.h>
#include <string.h>
#include "geometry.h"
#include "directionalLight.h"
#include "math.h"
#include "implemented.h"

int DirectionalLight::read(FILE* fp){
  if (fscanf(fp," %lg %lg %lg %lg %lg %lg",
	     &(color[0]),&(color[1]),&(color[2]),
	     &(direction[0]),&(direction[1]),&(direction[2])
	     ) != 6){return 0;}
  direction=direction.unit();
  return 1;
}
void DirectionalLight::write(FILE* fp){
  fprintf(fp,"#light_dir\n");
  fprintf(fp,"  %lg %lg %lg\n",color[0],color[1],color[2]);
  fprintf(fp,"  %lg %lg %lg\n",direction[0],direction[1],direction[2]);
}

Point3D DirectionalLight::getDiffuse(Point3D cameraPosition,
				     IntersectionInfo iInfo){
  if(implementedFlag){
    //printf("The DirectionalLight::getDiffuse(Point3D cameraPosition,IntersectionInfoiInfo) method has not been implemented\n");
  }
    return color;
}
Point3D DirectionalLight::getSpecular(Point3D cameraPosition,
				      IntersectionInfo iInfo){
  if(implementedFlag){
    //printf("The DirectionalLight::getSpecular(Point3D cameraPosition,IntersectionInfo iInfo) method has not been implemented\n");
  }
  Point3D origin= direction.negate();
  Point3D destination= cameraPosition-iInfo.iCoordinate;
  Point3D specular;
  Point3D h= origin+destination;
  h= h.unit();
  specular[0]= h[0]*color[0]; specular[1]=h[1]*color[1]; specular[2]= h[2]*color[2];
  specular.printnl();
  return specular;
}
int DirectionalLight::isInShadow(IntersectionInfo iInfo,Shape* shape){
  if(implementedFlag){
    //printf("The DirectionalLight::isInShadow(IntersectionInfo iInfo,Shape* shape) method has not been implemented\n");
  }
  Ray ray;
  IntersectionInfo info;
  ray.p= iInfo.iCoordinate; ray.d= direction.negate();
  if(shape->intersect(ray,info)>0)
    return 1;
  return 0;
}

