#ifndef RAY_INCLUDED
#define RAY_INCLUDED
#include "bmp.h"
#include "geometry.h"
#include "scene.h"

/***
 * This is the interface presented to main.c 
 * so that the ray tracer can be invoked.  
 * A ptr to the resulting image should be returned. 
 ***/

Image *RayTrace (
	const char *fileName, //ray file
	int width,            //image width				
	int height,           //image height
	int rLimit,           //max depth in recursion
	float cLimit          //minimum contribution threshold
);

Point3D GetColor (
	Scene scene,
	Ray ray,
	IntersectionInfo iInfo,
	int rDepth,
	float cLimit
);

#endif /* RAY_INCLUDED */
