#ifndef GROUP_INCLUDED
#define GROUP_INCLUDED
#include "shape.h"
#include "geometry.h"
#include "rayFileInstance.h"
/* The ShapeListElement class is used to store a list of shapes. It consists
 * of a pointer to the shape and a pointer to the next element of the list. */
class ShapeListElement {
 public:
  ShapeListElement(class Shape* shape);
  ShapeListElement* next;
  Shape* shape;
  void addShape(class Shape* newShape);
};

/* This Group class represents a node in the scene graph. It consists of a
 * list of shapes and a transformation that is to be applied to them. This
 * class is itself a subclass of Shape as it is ray-tracable. (A group is
 * ray-traced by ray-tracing each of its transformed children and obtaining
 * the closest point of intersection. Alternatively, and more simply, the
 * ray can be transformed appropriately and then one doesn't have to worry
 * about applying transformations to the child Shapes. */
class Group : public Shape{
 public:
  Matrix localTransform;
  ShapeListElement *shapeList;
  BoundingBox bBox;

    RayFileInstance *file_ray=NULL;
  Group(FILE* fp);

  Group(Matrix localTransform);

  void addShape(Shape* shape);

  char* name(void);

  void write(int indent,FILE* fp=stdout);
    int getCount();
  Flt intersect(Ray ray,IntersectionInfo& iInfo);

  BoundingBox getBoundingBox(void);

  void free(void);
};

#endif /* GROUP_INCLUDED */
