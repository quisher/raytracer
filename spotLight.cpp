#include <stdio.h>
#include <string.h>
#include "geometry.h"
#include "spotLight.h"
#include "math.h"
#include "implemented.h"

int SpotLight::read(FILE* fp){
  if (fscanf(fp," %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg",
	     &(color[0]),&(color[1]),&(color[2]),
	     &(location[0]),&(location[1]),&(location[2]),
	     &(direction[0]),&(direction[1]),&(direction[2]),
	     &(constAtten),&(linearAtten),&(quadAtten),
	     &(cutOffAngle),&(dropOffRate)) != 14){return 0;}
  direction=direction.unit();
  return 1;
}
void SpotLight::write(FILE* fp){
  fprintf(fp,"#light_spot\n");
  fprintf(fp,"  %lg %lg %lg\n",color[0],color[1],color[2]);
  fprintf(fp,"  %lg %lg %lg\n",location[0],location[1],location[2]);
  fprintf(fp,"  %lg %lg %lg\n",direction[0],direction[1],direction[2]);
  fprintf(fp,"  %lg %lg %lg\n",constAtten,linearAtten,quadAtten);
  fprintf(fp,"  %lg %lg\n",cutOffAngle,dropOffRate);
}
Point3D SpotLight::getDiffuse(Point3D cameraPosition,IntersectionInfo iInfo){
  if(implementedFlag){
   // printf("The SpotLight::getDiffuse(Point3D cameraPosition,IntersectionInfo iInfo) method has not been implemented\n");
  }
  //Point3D direction= iInfo.iCoordinate-location;
  Flt distance= (iInfo.iCoordinate.p[0]- location.p[0])*(iInfo.iCoordinate.p[0]- location.p[0])+
                (iInfo.iCoordinate.p[1]-location.p[1])*(iInfo.iCoordinate.p[1]-location.p[1])+
                (iInfo.iCoordinate.p[2]-location.p[2])*(iInfo.iCoordinate.p[2]-location.p[2]);
  Flt factor= direction.dot(iInfo.normal);
  Point3D diffusion(factor*color.p[0]/distance,factor*color.p[1]/distance,factor*color.p[2]/distance);
  //diffusion.printnl();
  return diffusion;
}
Point3D SpotLight::getSpecular(Point3D cameraPosition,IntersectionInfo iInfo){
  if(implementedFlag){
    //printf("The SpotLight::getSpecular(Point3D cameraPosition,IntersecitonInfo iInfo) method has not been implemented\n");
  }
  Point3D origin= location-iInfo.iCoordinate;
  Point3D destination= cameraPosition-iInfo.iCoordinate;
  Point3D specular;
  Point3D h= origin+destination;
  h= h.unit();
  Point3D light= getDiffuse(cameraPosition, iInfo);
  specular[0]= pow(h[0]*light[0],10000); specular[1]=pow(h[1]*light[1],10000); specular[2]= pow(h[2]*light[2],10000);
  return specular;
}
int SpotLight::isInShadow(IntersectionInfo iInfo,Shape* shape){
  if(implementedFlag){
    //printf("The SpotLight::isInShadow(IntersecitonInfo iInfo,Shape* shape) method has not been implemented\n");
  }
  Ray ray;
  IntersectionInfo info;
  ray.p= iInfo.iCoordinate; ray.d= location-iInfo.iCoordinate;
  if(shape->intersect(ray,info)>0)
    return 1;
  return 0;
}

