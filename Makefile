CC=gcc

SRCS=geometry.cpp main.cpp bmp.cpp scene.cpp box.cpp sphere.cpp cone.cpp cylinder.cpp triangle.cpp line.cpp \
ray.cpp group.cpp rayFileInstance.cpp pointLight.cpp spotLight.cpp directionalLight.cpp boundingBox.cpp \
implemented.cpp

OBJS=$(SRCS:.cpp=.o)

CCFLAGS=-DUSE_UNIX -g -Wall

.SUFFIXES: .cpp

tracer: $(OBJS)
	$(CC) -o tracer $(CCFLAGS) $(OBJS) -lm -lstdc++

.cpp.o:
	$(CC) $(CCFLAGS) -c $<

clean:
	-rm -f core
	-rm -f *.o
	-rm -f *~
	-rm -f viewer
	-rm -f tracer

