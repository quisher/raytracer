#include <stdio.h>
#include <stdlib.h>
#include "triangle.h"
#include "implemented.h"

Triangle::Triangle(FILE* fp,int* index,Vertex* vList,int vSize){
  int v1,v2,v3;
  if(fscanf(fp," %d %d %d %d",index,&v1,&v2,&v3) != 4){
    fprintf(stderr, "Failed to parse shape_triangle for Triangle\n");
    exit(EXIT_FAILURE);
  }
  if(v1<0 || v1>=vSize || v2<0 || v2>=vSize || v3<0 || v3>=vSize){
    fprintf(stderr, "Vertex index not within bounds for Triangle\n");
    exit(EXIT_FAILURE);
  }
  if(v1==v2 || v1==v3 || v2==v3){
    fprintf(stderr,"Vertices must be distinct in Triangle\n");
    exit(EXIT_FAILURE);
  }
  v[0]=&(vList[v1]);
  v[1]=&(vList[v2]);
  v[2]=&(vList[v3]);
}
void  Triangle::write(int indent,FILE* fp){
  int i;
  for(i=0;i<indent;i++){fprintf(fp," ");}
  fprintf(fp,"#shape_triangle %d\n",material->index);
  for(i=0;i<indent;i++){fprintf(fp," ");}
  fprintf(fp,"  %d %d %d\n",v[0]->index,v[1]->index,v[2]->index);
}
char* Triangle::name(void){return "triangle";}
Flt Triangle::intersect(Ray ray,IntersectionInfo& iInfo){
  if(implementedFlag){
    //printf("The Triangle::intersect(Ray ray,IntersectionInfo& iInfo) method has not been implemented\n");
  }
  //implementing triangle intersection using by centric coordinates.
    Point3D normal, Q; // nomral and Q is point inside triangle
    Point3D vertex0, vertex1, vertex2, temp, temp2, temp3;
    Flt nd, np, d, t, FltTemp; // NdotD NdotP and D is for formula n.x=d the step at which intersection happens
    vertex0= v[0]->position;
    vertex1= v[1]->position;
    vertex2= v[2]->position;
    temp= vertex1-vertex0;
    temp2= vertex2- vertex0;
    normal= temp.crossProduct(temp2);
    nd= normal.dot(ray.d);
    np= normal.dot(ray.p);
    if(nd==0) return -1;   // in this case ray is parallel and intersects at infinity
    d= normal.dot(vertex0);
    t= (d-np)/nd;
    Q= ray.position(t);
    // check if q is inside:
    temp= vertex1-vertex0;
    temp2= Q-vertex0;
    temp= temp.crossProduct(temp2);
    FltTemp= temp.dot(normal);
    if(FltTemp<0)
        return -1.0;
    temp= vertex2-vertex1;
    temp2= Q-vertex1;
    temp= temp.crossProduct(temp2);
    FltTemp= temp.dot(normal);
    if(FltTemp<0)
        return -1.0;
    temp=vertex0-vertex2;
    temp2= Q- vertex2;
    temp= temp.crossProduct(temp2);
    FltTemp= temp.dot(normal);
    if(FltTemp<0)
        return -1.0;
    iInfo.iCoordinate= Q;
    iInfo.material= material;
    iInfo.normal= normal.unit();
    return t;
  return -1.0;
}
BoundingBox Triangle::getBoundingBox(void){
  if(implementedFlag){
    printf("The Triangle::getBoundingBox(void) method has not been implemented\n");
  }
  return BoundingBox();
}

