#include <iostream>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "group.h"
#include "implemented.h"
//#include "rayFileInstance.h"

/* These are functions associated to the ShapeListElement class */
static int count;
ShapeListElement::ShapeListElement(Shape* s){
  next=NULL;
  shape=s;
}
void ShapeListElement::addShape(Shape* s){
  ShapeListElement* temp;
  temp=next;
  next=new ShapeListElement(s);
  assert(next);
  next->next=temp;
}

/* These are functions associated to the Group class */
Group::Group(FILE* fp){
    count++;
    printf("initialized \n");
  if(fscanf(fp," %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg",
	    &(localTransform(0,0)),&(localTransform(1,0)),
	    &(localTransform(2,0)),&(localTransform(3,0)),
	    &(localTransform(0,1)),&(localTransform(1,1)),
	    &(localTransform(2,1)),&(localTransform(3,1)),
	    &(localTransform(0,2)),&(localTransform(1,2)),
	    &(localTransform(2,2)),&(localTransform(3,2)),
	    &(localTransform(0,3)),&(localTransform(1,3)),
	    &(localTransform(2,3)),&(localTransform(3,3))) != 16){
    fprintf(stderr, "Failed to parse group_begin for Group\n");
    exit(EXIT_FAILURE);
  }
  shapeList=NULL;
}

int Group::getCount(){
    return count;
}

Group::Group(Matrix m){
    count++;
  localTransform=m;
  shapeList=NULL;
}
void Group::addShape(Shape* s){
  if(shapeList==NULL){shapeList=new ShapeListElement(s);}
  else{shapeList->addShape(s);}
}
char* Group::name(void){
    return "group";
    }
void Group::write(int indent,FILE* fp){
  ShapeListElement* temp;
  int i;

  for(i=0;i<indent;i++){fprintf(fp," ");}
  fprintf(fp,"#group_begin\n");
  for(i=0;i<indent;i++){fprintf(fp," ");}
  fprintf(fp,"  %lg %lg %lg %lg\n",
	  localTransform(0,0),localTransform(1,0),
	  localTransform(2,0),localTransform(3,0));
  for(i=0;i<indent;i++){fprintf(fp," ");}
  fprintf(fp,"  %lg %lg %lg %lg\n",
	  localTransform(0,1),localTransform(1,1),
	  localTransform(2,1),localTransform(3,1));
  for(i=0;i<indent;i++){fprintf(fp," ");}
  fprintf(fp,"  %lg %lg %lg %lg\n",
	  localTransform(0,2),localTransform(1,2),
	  localTransform(2,2),localTransform(3,2));
  for(i=0;i<indent;i++){fprintf(fp," ");}
  fprintf(fp,"  %lg %lg %lg %lg\n",
	  localTransform(0,3),localTransform(1,3),
	  localTransform(2,3),localTransform(3,3));
  temp=shapeList;
  while(temp!=NULL){
    temp->shape->write(indent+2,fp);
    temp=temp->next;
  }
  for(i=0;i<indent;i++){fprintf(fp," ");}
  fprintf(fp,"#group_end\n");
}
Flt Group::intersect(Ray ray,IntersectionInfo& iInfo){
  if(implementedFlag){
    //printf("The Group::intersect(Ray ray, IntersectionInfo* iInfo) method has not been implemented\n");
  }
  ShapeListElement* shapeListElem= shapeList;
  Shape* shape;
  Flt t;
  bool printingFlag=false;
    while(shapeListElem!=NULL){
        shape= shapeListElem->shape;
        //printf("it is a %s \n", shape->name());
        Group* grp= (Group*)shape;
        t=shape->intersect(ray, iInfo);
        if(t>0){
             return t;
        }
        if(printingFlag){
            int temp;
            std::cin>>temp;
        }
        shapeListElem= shapeListElem->next;
    }
  return -1.0;
}
BoundingBox Group::getBoundingBox(void){
  if(implementedFlag){
    //printf("The Group::getBoundingBox(void) method has not been implemented\n");
  }
  return BoundingBox();
}


void Group::free(void){
  ShapeListElement* temp;
  temp=shapeList;
  while(temp!=NULL){
    temp->shape->free();
    temp=temp->next;
  }
  delete this;
}
