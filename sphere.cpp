#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sphere.h"
#include "implemented.h"

Sphere::Sphere(FILE* fp,int* index){
  if(fscanf(fp," %d %lg %lg %lg %lg",index,
	    &(center[0]),&(center[1]),&(center[2]), &radius) != 5){
    fprintf(stderr, "Failed to parse shape_sphere for Sphere\n");
    exit(EXIT_FAILURE);
  }
}
char* Sphere::name(void){return "sphere";}
void  Sphere::write(int indent,FILE* fp){
  int i;
  for(i=0;i<indent;i++){fprintf(fp," ");}
  fprintf(fp,"#shape_sphere %d\n",material->index);
  for(i=0;i<indent;i++){fprintf(fp," ");}
  fprintf(fp,"  %lg %lg %lg\n",center[0],center[1],center[2]);
  for(i=0;i<indent;i++){fprintf(fp," ");}
  fprintf(fp,"  %lg\n",radius);
}
Flt Sphere::intersect(Ray ray,IntersectionInfo& iInfo){
  if(implementedFlag){
    //printf("The Sphere::intersect(Ray ray,IntersectionInfo& iInfo) method has not been implemented\n");
  }
//Compute A, B and C coefficients
    Flt a = ray.d.dot(ray.d);
    Flt b = 2 * ray.d.dot(ray.p-center);
    Flt c = ((ray.p-center).dot((ray.p-center)))-(radius*radius);
    Flt t=-1.0,t1,t2;
    //Find discriminant
    Flt disc = b * b - 4 * a * c;
    if(disc<0)
        return -1;
    t1= (-b+disc)/(2*a);
    t2= (-b-disc)/(2*a);
    if(t2>t1){

    }
    if(t1>t2)   t=t1;
    else    t=t2;
    if(t>0){
        iInfo.iCoordinate= ray.position(t);
        iInfo.material= material;
        iInfo.normal.p[0]= (iInfo.iCoordinate.p[0]- center.p[0])/radius;
        iInfo.normal.p[1]= (iInfo.iCoordinate.p[1]- center.p[1])/radius;
        iInfo.normal.p[2]= (iInfo.iCoordinate.p[2]- center.p[2])/radius;
        return t;
    }
  return -1.0;
}
BoundingBox Sphere::getBoundingBox(void){
  if(implementedFlag){
    printf("The Sphere::getBoundingBox(void) method has not been implemented\n");
  }
  return BoundingBox();
}
