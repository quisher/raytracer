#include <stdio.h>
#include <stdlib.h>
#include "cylinder.h"
#include "implemented.h"

Cylinder::Cylinder(FILE* fp,int* index){
  if(fscanf(fp," %d %lg %lg %lg %lg %lg",index,
	    &(center[0]),&(center[1]),&(center[2]),
	    &radius,&height) != 6){
    fprintf(stderr, "Failed to parse shape_cylinder for Cylinder\n"); 
    exit(EXIT_FAILURE);
  }
}
char* Cylinder::name(void){return "cylinder";}
void  Cylinder::write(int indent,FILE *fp){
  int i;

  for(i=0;i<indent;i++){fprintf(fp," ");}

  fprintf(fp,"#shape_cylinder %d\n",material->index);

  for(i=0;i<indent;i++){fprintf(fp," ");}

  fprintf(fp,"  %lg %lg %lg\n",center[0],center[1],center[2]);

  for(i=0;i<indent;i++){fprintf(fp," ");}

  fprintf(fp,"  %lg %lg\n",radius,height);
}

Flt Cylinder::intersect(Ray ray,IntersectionInfo& iInfo){
  if(implementedFlag){
    printf("The Cylinder::intersect(Ray ray,IntersectionInfo& iInfo) method has not been implemented\n");
  }
  return -1.0;
}

BoundingBox Cylinder::getBoundingBox(void){
  if(implementedFlag){
    printf("The Cylinder::getBoundingBox(void) method has not been implemented\n");
  }
  return BoundingBox();
}

