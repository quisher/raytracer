#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ray.h"
#include "bmp.h"

/* Default values if cmd line options are missing */

#define DEFAULT_WIDTH 100
#define DEFAULT_HEIGHT 100
#define DEFAULT_RLIM 5
#define DEFAULT_CLIM 0.001

#define OS_UNIX 1
#define OS_WINNT 2

#ifdef USE_UNIX
#   define OS OS_UNIX
#else
#   define OS OS_WINNT
#endif

#if OS == OS_WINNT
#   include <io.h>
#   include <fcntl.h>
#endif

static char *progname;

static void ShowUsage(void);
static void SetBinaryIOMode(void);


int main(int argc, char **argv)
{
  char *dst_filename = NULL, *src_filename = NULL;
  int width = DEFAULT_WIDTH, height = DEFAULT_HEIGHT;
  int rLimit = DEFAULT_RLIM;
  float cLimit = (float)DEFAULT_CLIM;
  Image *img = NULL;
  FILE *fp = NULL;

  SetBinaryIOMode();
  progname = argv[0];
  if (argc < 3) ShowUsage();

  argv++, argc--;
  while (argc > 0) {
    if (**argv == '-') {

      if (argc < 2) {
	fprintf(stderr, "Not enough arguments for %s\n", argv[0]);
	ShowUsage();
      }
      else if(!strcmp(*argv, "-help")){ShowUsage(); }
      else if(!strcmp(*argv, "-width")){width = atoi(argv[1]);}
      else if(!strcmp(*argv, "-height")){height = atoi(argv[1]);}
      else if(!strcmp(*argv, "-rlim")){rLimit = atoi(argv[1]);}
      else if(!strcmp(*argv, "-clim")){cLimit = (float)atof(argv[1]);}
      else if(!strcmp(*argv, "-src")){src_filename = argv[1];}
      else if(!strcmp(*argv, "-dst")){dst_filename = argv[1];}
      else{
	fprintf(stderr, "%s: invalid option: %s\n", progname, *argv);
	ShowUsage();
      }
      argv++, argc--;
      argv++, argc--;
    }
    else {
      fprintf(stderr, "%s: invalid option: %s\n", progname, *argv);
      ShowUsage();
    }
  }

  if ((dst_filename==NULL)||(src_filename==NULL)) {
    fprintf(stderr, "%s: a source and destination file must be defined!\n",
	    progname);
    exit(EXIT_FAILURE);
  }

  /*Call the raytracer with the provided arguments*/

  img = RayTrace (src_filename, width, height, rLimit, cLimit);

  /*Write the resulting image to a file */
  fp=fopen(dst_filename, "wb");
  assert(fp != NULL);
  BMPWriteImage(img, fp);
  ImageFree(&img);
  return EXIT_SUCCESS;
}




static char options[] =
"-width <number of pixels>\n"
"-height <number of pixels>\n"
"-rlim <recursion limit>\n"
"-clim <minimum contribution limit>\n"
"-help\n"
;



static void ShowUsage(void)
{
  fprintf(stderr, "Usage: %s [-option [arg ...] ...] -src <input .ray file> -dst <output .bmp file>\n", progname);
  fprintf(stderr, options);
  exit(EXIT_FAILURE);
}


static void SetBinaryIOMode(void)
{
#if OS == OS_WINNT
  int result;
  result = _setmode(_fileno(stdin), _O_BINARY);
  if (result == -1) {
    perror("Cannot set stdin binary mode");
    exit(EXIT_FAILURE);
  }
  result = _setmode(_fileno(stdout), _O_BINARY);
  if (result == -1) {
    perror("Cannot set stdout binary mode");
    exit(EXIT_FAILURE);
  }
#endif
}
