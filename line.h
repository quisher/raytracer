#ifndef LINE_INCLUDED
#define LINE_INCLUDED
#include "geometry.h"
#include "shape.h"

/* This class represents a line segment and is specified by its starting and
 * ending points. */

class Line : public Shape {
  Point3D start,end;
 public:
  Line(FILE* fp,int* materialIndex);

  Line(Ray ray,Material* material);

  char* name(void);

  void write(int indent,FILE* fp=stdout);

  Flt intersect(Ray ray,IntersectionInfo& iInfo);

  BoundingBox getBoundingBox(void);

  void draw(int complexity);
};

#endif /* LINE_INCLUDED */
  
