#ifndef BOX_INCLUDED
#define BOX_INCLUDED
#include "shape.h"
#include "geometry.h"

/* This class represents a box and is defined by its center and the length of
 * the sides. */

class Box : public Shape {
 public:
  Point3D center;
  Point3D length;

  Box(void);
  Box(Point3D center, Point3D length);
  Box(FILE* fp,int* materialIndex);

  char* name(void);

  void write(int indent,FILE* fp=stdout);

  Flt intersect(Ray ray,IntersectionInfo& iInfo);

  BoundingBox getBoundingBox(void);
};

#endif /* BOX_INCLUDED */
  
