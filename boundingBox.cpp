#include <stdio.h>
#include <math.h>
#include "boundingBox.h"
#include "implemented.h"

void BoundingBox::setBoundingBox(Point3D p1,Point3D p2){
  p[0]=Point3D((p1[0]<=p2[0])? p1[0]:p2[0],
	       (p1[1]<=p2[1])? p1[1]:p2[1],
	       (p1[2]<=p2[2])? p1[2]:p2[2]);
  p[1]=Point3D((p1[0]>=p2[0])? p1[0]:p2[0],
	       (p1[1]>=p2[1])? p1[1]:p2[1],
	       (p1[2]>=p2[2])? p1[2]:p2[2]);
}
BoundingBox::BoundingBox(void){
  p[0]=Point3D();
  p[1]=Point3D();
}
BoundingBox::BoundingBox(Point3D p1,Point3D p2){setBoundingBox(p1,p2);}
BoundingBox::BoundingBox(Point3D* pList,int pSize){
  if(implementedFlag){
    printf("The BoundingBox::BoundingBox(Point3D* pList,int pSize) constructor has not been implemented\n");
  }
}
BoundingBox BoundingBox::operator+ (BoundingBox b){
  if(implementedFlag){
    printf("The BoundingBox::operator+ (BoundingBox b) method has not been implemented\n");
  }
  return BoundingBox();
}

Flt BoundingBox::intersect(Ray ray){
  if(implementedFlag){
    printf("The BoundingBox::intersect(Ray ray) method has not been implemented\n");
  }
  return -1.0;
}
BoundingBox BoundingBox::transform(Matrix m){
  if(implementedFlag){
    printf("The BoundingBox::transform(Matrix m) method has not been implemented\n");
  }
  return BoundingBox();
}


