#ifndef SHAPE_INCLUDED
#define SHAPE_INCLUDED
#include <stdio.h>
#include "geometry.h"
#include "scene.h"
#include "boundingBox.h"

/* 
 * This is the interface that all the objects must implement. 
 * Objects must have accessable matrices to give the transformation to and
 * from world coordinates.
 * Objects must support reading, writing and printing.
 * Objects must support intersection with rays and they must be able to
 * return their bounding boxes.
 */

class Shape {
 public:
  class Material *material;

  /* These functions are the basic I/O functions for the object */
  virtual char* name(void)=0;
  virtual void write(int indent,FILE* fp=stdout)=0;

  /* This function should update the isect info as appropriate and return
   * the distance to the shape along the specified ray. If the ray does not
   * intersect the shape, the function should return a negative number */
  virtual Flt intersect(Ray ray, struct IntersectionInfo& iInfo)=0;

  /* This function returns a bounding box containing the Shape.*/
  virtual BoundingBox getBoundingBox(void)=0;

  void free(void){;}
};

struct IntersectionInfo {
  Material* material;
  Point3D iCoordinate;
  Point3D normal;
  Point2D texCoordinate;
};

#endif /* SHAPE_INCLUDED */
