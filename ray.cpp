#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "group.h"
#include "ray.h"
#include "line.h"
#include "sphere.h"
#include "cone.h"
#include "box.h"
#include "cylinder.h"
#include "triangle.h"
#include "bmp.h"
#include "implemented.h"

/****
 *
 * The meat of the ray tracer is in this file.
 * You should implement your controlling code
 * in the RayTrace function, and by adding
 * new functions to this file.
 * The other significant ray tracing code
 * is in the primitive .c files (box, cone, etc.)
 *
 ****/


/****
 *
 * The globals are all set by the parser
 * If you distate global variables,
 * you might move some of these into
 * RayTrace()
 *
 ****/


/****
 * A call to this function invokes the ray tracer. It returns
 * a pointer to the image when it's done.
 ***/

Image* RayTrace(const char *fileName,int width,int height,
		int rLimit, float cLimit){
  Scene scene;
  Image *img;
  Group *group;
  IntersectionInfo info;
  Point3D color;
  Ray *ray;
  /* For the parsing we turn of the implemented flag */
  implementedFlag=0;

  /* The image width/height determines the aspect ratio
   * This ratio times the height angle gives the width angle
   * of the viewing volulme. */

  scene.camera.aspectRatio = (Flt)width/(Flt)height;

  img = ImageNew(width,height);

  /* This parses the entire scene graph into a node tree structure containing
   * the primitive shaped and the transform hierarchy at each node
   * The parser also mallocs lottsa space for materials, lights, and textures.
   * The camera and environment structures are also filled.*/
  scene.read(fileName);

  /* After parsing we turn the implemented flag on to announce when an
   * unimplemented method is being invoked. */
  implementedFlag=1;

  /* A debugging tool. This function prints
   * and indented tree of the scene graph
   * to the screen. Coordinates and matrices
   * are given.*/
  scene.shape->write(0);

  /****
   *
   *Ray tracing here
   *
   ****/
    Point3D *center= new Point3D(scene.camera.direction.p[0]-scene.camera.position.p[0],scene.camera.direction.p[1]-scene.camera.position.p[1],scene.camera.direction.p[2]-scene.camera.position.p[2]);
    int x,y;
    x=0;y=0;
   for(int i=center->p[0]-width/2;i < ((center->p[0]+width/2)); i++){
    y=0;
    for(int j=center->p[1]-height/2; j< ((center->p[1]+height/2));j++){
            Point3D *pt;
            pt= new Point3D(scene.camera.position.p[0], scene.camera.position.p[1], scene.camera.position.p[2]);
           // oldPt= new Point3D(scene.camera.position.p[0],scene.camera.position.p[1],scene.camera.position.p[2]);

            Point3D direction;
            direction.p[0]=i;
            direction.p[1]=j;
            direction.p[2]=center->p[2];
            ray= new Ray(*pt, direction);
            if(scene.shape->intersect(*ray, info)>0){
                //printf("%d %d \n",x ,y);
                Pixel p; p.r=255; p.b=255; p.g=255;
                color= GetColor(scene, *ray, info, rLimit, cLimit);
                p.r= p.r*color.p[0]; p.b=p.b*color.p[1]; p.g= p.g*color.p[2];
                //ray->d.printnl();
                //printf("%d %d \n",x,y);
                ImageSetPixel(img, x, y, &p);
            }
            y++;
        //}
    }
    x++;
   }

  /* Clean up is taken care of for you :') */
  scene.free();
  return img;
}

/*//////////////////////////////////////*/
Point3D GetColor ( Scene scene, Ray ray, IntersectionInfo iInfo, int rDepth, float cLimit)
{
    Point3D color;
    color= iInfo.material->ambient.mult(scene.ambient);
    color= color.add(iInfo.material->emissive);
    for(int i=0; i<scene.lightNum; i++){
            color=color.add(scene.lights[i]->getDiffuse(scene.camera.position, iInfo));
            color=color.add(scene.lights[i]->getDiffuse(scene.camera.position, iInfo));
            color= color.add(scene.lights[i]->getSpecular(scene.camera.position, iInfo));
            if(scene.lights[i]->isInShadow(iInfo,scene.shape)>0){
            }
            else{
                printf("Has shadow \n");
            }
            Ray ray;
            ray.p=iInfo.iCoordinate;
            ray.d= (iInfo.iCoordinate-scene.camera.position).unit();
            Point3D normal= iInfo.normal.unit();
            Point3D direction= normal.scale(2* (normal.dot(ray.d)))-ray.d;
            ray.d= direction;
            if(scene.shape->intersect(ray, iInfo)>0){
//                color= iInfo.material->ambient.mult(scene.ambient);
//                color= color+iInfo.material->emissive;
            }
    }
    return color; /* for now, return white */
}
