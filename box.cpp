#include <stdio.h>
#include <stdlib.h>
#include "box.h"
#include "implemented.h"

Box::Box(void){
  center=Point3D();
  length=Point3D();
}
Box::Box(Point3D center, Point3D length){
  this->center=center;
  this->length=length;
}
Box::Box(FILE* fp,int* index){
  if(fscanf(fp," %d %lg %lg %lg %lg %lg %lg",index,
	    &(center[0]),&(center[1]),&(center[2]),
	    &(length[0]),&(length[1]),&(length[2])) != 7){
    fprintf(stderr, "Failed to parse shape_box for Box\n"); 
    exit(EXIT_FAILURE);
  }
}

char* Box::name(void){return "box";}
void  Box::write(int indent,FILE* fp){
  int i;

  for(i=0;i<indent;i++){fprintf(fp," ");}

  fprintf(fp,"#shape_box %d\n",material->index);

  for(i=0;i<indent;i++){fprintf(fp," ");}

  fprintf(fp,"  %lg %lg %lg\n",center[0],center[1],center[2]);

  for(i=0;i<indent;i++){fprintf(fp," ");}

  fprintf(fp,"  %lg %lg %lg\n",length[0],length[1],length[2]);
}

Flt Box::intersect(Ray ray,IntersectionInfo& iInfo){
  if(implementedFlag){
    printf("The Box::intersect(Ray ray,IntersectionInfo& info) method has not been implemented\n");
  }
  return -1.0;
}

BoundingBox Box::getBoundingBox(void){
  if(implementedFlag){
    printf("The Box::getBoundingBox(void) method has not been implemented\n");
  }
  return BoundingBox();
}


  
