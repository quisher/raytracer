#include <stdio.h>
#include <stdlib.h>
#include "cone.h"
#include "implemented.h"

Cone::Cone(FILE* fp,int* index){
  if(fscanf(fp," %d %lg %lg %lg %lg %lg",index,
	    &(center[0]),&(center[1]),&(center[2]),
	    &radius,&height) != 6){
    fprintf(stderr, "Failed to parse shape_cone for Cone\n"); 
    exit(EXIT_FAILURE);
  }
}
char* Cone::name(void){return "cone";}
void  Cone::write(int indent,FILE* fp){
  int i;

  for(i=0;i<indent;i++){fprintf(fp," ");}

  fprintf(fp,"#shape_cone %d\n",material->index);

  for(i=0;i<indent;i++){fprintf(fp," ");}

  fprintf(fp,"  %lg %lg %lg\n",center[0],center[1],center[2]);

  for(i=0;i<indent;i++){fprintf(fp," ");}

  fprintf(fp,"  %lg %lg\n",height, radius);
}

Flt Cone::intersect(Ray ray,IntersectionInfo& iInfo){
  if(implementedFlag){
    printf("The Cone::intersect(Ray ray,IntersectionInfo& iInfo) method has not been implemented\n");
  }
  return -1.0;
}

BoundingBox Cone::getBoundingBox(void){
  if(implementedFlag){
    printf("The Cone::getBoundingBox(void) method has not been implemented\n");
  }
  return BoundingBox();
}


