#ifndef SCENE_INCLUDED
#define SCENE_INCLUDED
#include "bmp.h"
#include "geometry.h"
#include "shape.h"
#include "light.h"

#define TEX_FNAME_BUF_SIZE 200
#define FOO_STRING_BUF_SIZE 200
#define FILE_NAME_SIZE 200

/***
 * This class stores information about the location and direction of a camera
 * in the scene. 
 ***/

class Camera {
 public:
  Flt heightAngle;
  Flt aspectRatio;
  Point3D position;
  Point3D direction;
  Point3D up;
  Point3D right;

  int read(FILE* fp);
  void write(FILE* fp=stdout);
};

/***
 * This class stores information about a vertex that is used by the 
 * triangles in the scene.
 ***/

class Vertex {
 public:
  int index;
  Point3D position;
  Point3D normal;
  Point2D texCoordinate;
  int read(FILE* fp);
  void write(FILE* fp=stdout);
};


/***
 * This class stores information about a material describing shapes in the
 * scene. (This includes a pointer to a Texture if the Shape is to be
 * texture mapped.)
 ***/

class Material {
 public:
  int index;
  Point3D ambient;
  Point3D diffuse;
  Point3D specular;
  Point3D emissive;
  Flt kspec;
  Flt ktran;
  Flt refind;
  class Texture *tex;
  char foo[FOO_STRING_BUF_SIZE];

  int read(FILE* fp,int& textureIndex);
  void write(FILE* fp=stdout);
};

/* This class stores information about a texture used to texture-map Shapes 
 * in the scene. */

class Texture{
 public:
  int index;
  char filename[TEX_FNAME_BUF_SIZE];
  Image *img;

  int read(FILE* fp);
  void write(FILE* fp=stdout);
};

/* This class stores information about the scene-graph read out from a .ray
 * file. */

class RayFile{
 public:
  int index;
  char filename[FILE_NAME_SIZE];
  class Scene* scene;

  int read(FILE* fp);
  void write(FILE* fp=stdout);
  void free(void);
};

/* This class provides access to everything described in the .ray file and
 * stores a copy of the geometry of the scene in the form of a scene-graph. */
class Scene {
  Vertex* vertices;
  int vertexNum;
  Material* materials;
  int materialNum;
  Texture* textures;
  int textureNum;
  RayFile* rayFiles;
  int rayFileNum;
  void ParseError(const char *, const char *);
  void ParseLineError(int ,const char*, const char *);
  void ParseGroup(FILE* fp,int& cmndCtr,class Shape* current,
		  const char* fileName);
  int  ParseShape(FILE* fp,const char* keyword,int cmndCtr,
		  class Shape* current,const char* fileName);
 public:
  Point3D ambient;
  Point3D background;
  Camera camera;
  class Light** lights;
  int lightNum;
  Shape* shape;

  Scene(void);

  Material* getMaterial(int index);

  void setUp(void);
  void free(void);

  void read(const char* fileName);
  void write(FILE* fp=stdout);
};

#endif /* SCENE_INCLUDED */
  

