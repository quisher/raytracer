#ifndef TRIANGLE_INCLUDED
#define TRIANGLE_INCLUDED
#include "shape.h"
#include "geometry.h"
#include "scene.h"

/* This class represents a triangle and is specified by three pointers to
 * the three vertices that define it. */

class Triangle : public Shape {
 public:
  class Vertex* v[3];
  Triangle(FILE* fp,int* materialIndex,Vertex* vList,int vSize);

  char* name(void);

  void write(int indent,FILE* fp=stdout);

  Flt intersect(Ray ray,IntersectionInfo& iInfo);

  BoundingBox getBoundingBox(void);
};
#endif /* TRIANGLE_INCLUDED */
  

