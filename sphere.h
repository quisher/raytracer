#ifndef SPHERE_INCLUDED
#define SPHERE_INCLUDED
#include "shape.h"
#include "geometry.h"

/* This class describes a sphere, defined by its center point and its radius.
 */

class Sphere : public Shape {
 public:
  Point3D center;
  Flt radius;

  Sphere(FILE* fp,int* materialIndex);

  char* name(void);

  void write(int indent,FILE* fp=stdout);

  Flt intersect(Ray ray,IntersectionInfo& iInfo);


  BoundingBox getBoundingBox(void);
};

#endif /* SPHERE_INCLUDED */

