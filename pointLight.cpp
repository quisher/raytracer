#include <stdio.h>
#include <string.h>
#include "geometry.h"
#include "pointLight.h"
#include "math.h"
#include "implemented.h"

int PointLight::read(FILE* fp){
  if(fscanf(fp," %lg %lg %lg %lg %lg %lg %lg %lg %lg",
	    &(color[0]),&(color[1]),&(color[2]),
	    &(location[0]),&(location[1]),&(location[2]),
	    &(constAtten),&(linearAtten),&(quadAtten)) != 9){return 0;}
  return 1;
}
void PointLight::write(FILE* fp){
  fprintf(fp,"#light_point\n");
  fprintf(fp,"  %lg %lg %lg\n",color[0],color[1],color[2]);
  fprintf(fp,"  %lg %lg %lg\n",location[0],location[1],location[2]);
  fprintf(fp,"  %lg %lg %lg\n",constAtten,linearAtten,quadAtten);
}
Point3D PointLight::getDiffuse(Point3D cameraPosition,IntersectionInfo iInfo){
  if(implementedFlag){
    //printf("The PointLight::getDiffuse(Point3D cameraPosition,IntersectionInfo iInfo) method has not been implemented\n");
  }
  Flt distance= (iInfo.iCoordinate.p[0]- location.p[0])*(iInfo.iCoordinate.p[0]- location.p[0])+
                (iInfo.iCoordinate.p[1]-location.p[1])*(iInfo.iCoordinate.p[1]-location.p[1])+
                (iInfo.iCoordinate.p[2]-location.p[2])*(iInfo.iCoordinate.p[2]-location.p[2]);
  Point3D diffusion(color.p[0]/distance,color.p[1]/distance,color.p[2]/distance);
  return diffusion;
}
Point3D PointLight::getSpecular(Point3D cameraPosition,IntersectionInfo iInfo){
  if(implementedFlag){
    //printf("The PointLight::getSpecular(Point3D cameraPosition,IntersectionInfo iInfo) method has not been implemented\n");
  }
  Point3D origin= location-iInfo.iCoordinate;
  Point3D destination= cameraPosition-iInfo.iCoordinate;
  Point3D specular;
  Point3D h= origin+destination;
  h= h.unit();
  Point3D light= getDiffuse(cameraPosition, iInfo);
  specular[0]= pow((h[0]*light[0]),100); pow(specular[1]=h[1]*light[1],100); pow(specular[2]= h[2]*light[2],100);
  specular.printnl();
  return specular;
}
int PointLight::isInShadow(IntersectionInfo iInfo,Shape* shape){
  if(implementedFlag){
    //printf("The PointLight::isInShadow(IntersectionInfo iInfo,Shape* shape) method has not been implemented\n");
  }
  Ray ray;
  IntersectionInfo info;
  ray.p= iInfo.iCoordinate; ray.d= location-iInfo.iCoordinate;
  if(shape->intersect(ray,info)>0)
    return 1;
  return 0;
}

